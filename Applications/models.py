from django.db import models

# Create your models here.

class Application(models.Model):
    CategoriesList= [
    ("Flutter", "Flutter"),
    ("Django", "Django"),
]
    title =models.CharField(max_length=50)
    description = models.TextField()
    category = models.CharField(max_length=50,choices=CategoriesList)
    dateCreated = models.DateTimeField()

    def __str__(self):
        return self.title
    

class Image(models.Model):
    app = models.ForeignKey(Application,on_delete=models.CASCADE)
    img = models.FileField()

