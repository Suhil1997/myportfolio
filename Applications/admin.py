from . import models
from django.contrib import admin

# Register your models here.
@admin.register(models.Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display =['id','title']

@admin.register(models.Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ['image_filename','app_title']
    
    def image_filename(self, obj):
        return obj.img.name.split('/')[-1]

    def app_title(self, obj):
       return obj.app.title