from django.urls import  path
from django.conf.urls.static import static
from Applications import views
from django.conf import settings

urlpatterns = [
    path('', views.App,name='apps'),
    path('applications/<int:pk>', views.AppDetail,name='app_detail'),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)