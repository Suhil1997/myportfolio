from django.shortcuts import render
from django.shortcuts import redirect, render
from .models import Application
# Create your views here.

def AppDetail(request,pk):
    application = Application.objects.get(pk=pk)
    context ={'application':application}
    return render(request,'app_detail.html',context)

def App(request):
    applications = Application.objects.all()
    context ={'applications':applications}
    return render(request,'index.html',context)