from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def ext_check(value, extensions):
    if not value:
        return False
    ext = value.split('.')[-1]  
    return ext.lower() in extensions.lower().split(',')